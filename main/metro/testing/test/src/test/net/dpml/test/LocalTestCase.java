/*
 * Copyright 2006 Stephen J. McConnell.
 *
 * Licensed  under the  Apache License,  Version 2.0  (the "License");
 * you may not use  this file  except in  compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.dpml.test;

import dpml.util.DefaultLogger;

import java.net.URI;
import java.net.URL;

import junit.framework.TestCase;

import net.dpml.appliance.Appliance;

import net.dpml.transit.Artifact;

import net.dpml.util.Logger;

/**
 * Appliance testcase.
 * @author <a href="@PUBLISHER-URL@">@PUBLISHER-NAME@</a>
 * @version @PROJECT-VERSION@
 */
public class LocalTestCase extends TestCase
{
    private Logger m_logger = new DefaultLogger( "test" );

   /**
    * Test appliance deployment via appliance uri content resolution.
    * @exception Exception if an error occurs
    */
    public void testClassicAppliance() throws Exception
    {
        URI uri = URI.create( "link:appliance:dpml/metro/demo" );
        URL url = Artifact.toURL( uri );
        m_logger.info( "Creating appliance." );
        Appliance appliance = null;
        try
        {
            appliance = (Appliance) url.getContent( new Class[]{Appliance.class} );
            m_logger.info( "Appliance created." );
        }
        catch( Throwable e )
        {
            final String msg = "Appliance creation failure (" + url.toString() + ")";
            m_logger.error( msg, e );
            throw new Exception( msg, e );
        }
        
        try
        {
            try
            {
                m_logger.info( "Commissioning appliance." );
                appliance.commission();
            }
            catch( Throwable e )
            {
                final String msg = "Commissioning failure";
                m_logger.error( msg, e );
                throw new Exception( msg, e );
            }
        }
        finally
        {
            try
            {
                m_logger.info( "Decommissioning appliance." );
                appliance.decommission();
            }
            catch( Throwable e )
            {
                final String msg = "Decommissioning failure";
                m_logger.error( msg, e );
                throw new Exception( msg, e );
            }
        }
    }

    static
    {
        System.setSecurityManager( new SecurityManager() );
    }
}
