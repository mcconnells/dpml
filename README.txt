This is the DPML repository.
(replicated from SVN - no history)

D:\>cd dpml
D:\dpml>git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   README.txt

no changes added to commit (use "git add" and/or "git commit -a")

D:\dpml>git add .

D:\dpml>git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   README.txt

D:\dpml>git commit -m "my first remote commit"
[master 420bfaf] my commit
 1 file changed, 2 insertions(+), 1 deletion(-)

D:\dpml>git push origin master
Password for 'https://mcconnells@gitlab.com':
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 305 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://gitlab.com/mcconnells/dpml.git
   b94d27f..420bfaf  master -> master

D:\dpml>

EOF